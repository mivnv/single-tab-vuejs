  chrome.browserAction.onClicked.addListener(() => {
    let closedTabs = [];

    chrome.tabs.query({}, (tabs) => {

      chrome.tabs.create({
        'url': chrome.extension.getURL('index.html')
      }, (newTab) => {

        for (let i = 0; i < tabs.length; i++) {
          let notCurrentTab = newTab.id !== tabs[i].id;
          let notNewTab = tabs[i].url.indexOf("chrome://newtab") === -1;
          let notExtPage = tabs[i].url.indexOf(newTab.url) === -1;

          if (notCurrentTab && notNewTab && notExtPage) {
            closedTabs.push(tabs[i]);
          }
          chrome.tabs.remove(tabs[i].id);
        }

        chrome.tabs.onUpdated.addListener((tabId, changeInfo) => {
          if (newTab.id === tabId && changeInfo.status === 'complete') {
            let db;
            let request = indexedDB.open("tabsData", 1);

            request.onupgradeneeded = (event) => {
              db = event.target;

              let objectStore = db.result.createObjectStore("tabsets", {
                keyPath: "id"
              });

              chrome.runtime.sendMessage({
                closedTabs: closedTabs,
                id: +new Date(),
                tabsetName: ''
              });

              closedTabs = [];
            };

            request.onsuccess = (event) => {
              if (!db) {

                chrome.runtime.sendMessage({
                  closedTabs: closedTabs,
                  id: +new Date(),
                  tabsetName: ''
                });

                closedTabs = [];
              };

            };

          };

        });

      });

    });

  });