window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;

chrome.runtime.onMessage.addListener((tabset) => {
  const request = indexedDB.open("tabsData", 1);

  request.onsuccess = (event) => {
    let db = request.result;
    let tx = db.transaction("tabsets", "readwrite");
    let store = tx.objectStore("tabsets");

    if (tabset.closedTabs.length) {
      tabset.closedTabs = tabset.closedTabs.map((tab) => {

        if (!tab.favIconUrl || tab.favIconUrl === "") {
          tab.favIconUrl = "https://cdn3.iconfinder.com/data/icons/faticons/32/globe-01-256.png"
        }

        return tab;
      });

      store.put({
        id: tabset.id,
        tabs: tabset.closedTabs,
        createdAt: new Date(),
        tabsetName: ''
      });

    }

    let fullTabsetData = store.getAll();
    fullTabsetData.onsuccess = (event) => {

      let allTabsetsData = event.target.result.reverse();

      Vue.use(VueMaterial.default)
      Vue.component('tabsetcomponent', {
        props: ["tabset"],
        data: function() {
          return {
            isFocused: false,
            tabsetName: this.tabset.tabsetName,
            hoveredTabId: 0,
            blockTabset: false
          }
        },
        template: `<div style="list-style-type: none;">
                      <md-list>
                        <md-list-item>
                          <div class='topBlock'>
                             <md-icon class='blockIcon' v-on:click.native="toggleBlockTabset()">{{this.blockTabset? 'lock' : 'lock_open'}}</md-icon>
                             <md-field v-show=isFocused :style="{width: tabsetName.length > 0 ? (tabsetName.length + 1) * 10 + 'px' :'15px'}">
                                  <md-input t-id="tabset.id" v-on:blur="renameTabset(tabset)" v-on:keyup.13="blurInput" :value="tabsetName" autofocus maxlength='15' class='input' md-counter="0" v-model="tabsetName"></md-input>
                              </md-field>
                            <div class='tabsetNameGroup'>
                              <span class="md-display-1 inputText"  v-on:click="renameTabset(null, $event)" :class="{ focused: isFocused }">{{ tabset.tabsetName || this.tabsetName || tabset.tabs.length + ' Tabs' }}</span>
                            </div>
                          </div>
                        </md-list-item>
                          <span class='createdAt'>Created at {{ tabset.createdAt.toLocaleDateString() + ', ' + tabset.createdAt.toLocaleTimeString()}}</span>
                        <md-list-item v-for="tab in tabset.tabs" :key="tab.id">

                            <div class="tab" v-on:mouseover="hoverTab(tab.id)"
                                             v-on:mouseleave="hoveredTabId = -1"
                                             :class="{loadingTab: tab.status === 'loading'}">
                              <md-icon v-on:click.native='removeUrl(tab, tabset)' style="visibility: hidden;" :class="{ activeClearIcon: tab.id === hoveredTabId && !blockTabset }">clear</md-icon>
                              <img class="favIcons" :class="{hideTabIcon: tab.status === 'loading'}" :src="tab.favIconUrl"/>&nbsp;
                              <md-progress-spinner md-mode="indeterminate" style="display:none"
                                                                           :md-diameter="16"
                                                                           :md-stroke="3">
                                                                           </md-progress-spinner>
                              <a class="tabs-links" target="_blank" :href="tab.url" v-on:click="removeUrl(tab, tabset)">{{ tab.title }}</a>
                            </div>

                        </md-list-item>
                        <md-list-item>
                          <div class="bottomToolbar">
                            <div class="buttonsWrapper">
                              <md-button class="md-raised md-primary" v-on:click="restoreTabset(tabset)">Restore tabset</md-button>
                              <md-button class="md-raised md-accent" :disabled="blockTabset" v-on:click="deleteTabset(tabset.id)"> Delete tabset </md-button>
                            </div>
                          </div>
                        </md-list-item>
                      </md-list>
                      <hr>
                   </div>`,
        methods: {
          deleteTabset: function(id) {
            let deleteReq = db.transaction("tabsets", "readwrite").objectStore("tabsets").delete(id);

            deleteReq.onsuccess = function() {
              vm.allTabsets = vm.allTabsets.filter((tabset) => tabset.id !== id);
            }

          },

          renameTabset: function(tabset, event) {
            if (tabset && this.isFocused) {
              tabset.tabsetName = this.tabsetName;
              this.updatedTabset(tabset);
              this.isFocused = false;
            } else {
              this.isFocused = true;
              //this.$refs.search.focus();
              window.test = this
              //this.$refs.tabsetName.$el.focus()  
            }
          },

          removeUrl: function(tab, tabset) {
            if (!this.blockTabset) {
              tabset.tabs = tabset.tabs.filter((val) => val.id !== tab.id);
              this.updatedTabset(tabset);

              if (tabset.tabs.length < 1) {
                this.deleteTabset(tabset.id);
              }
            }
            //vm.allTabsets = vm.allTabsets.map((tabset_) => tabset_.id !== updatedTabset.id ? tabset_ : updatedTabset);
          },

          hoverTab: function(tabsId) {
            this.hoveredTabId = tabsId;
          },

          toggleBlockTabset: function(id) {
            this.style = this.style === '' ? 'none' : '';
            this.blockTabset = !this.blockTabset
          },

          restoreTabset: function(tabset) {
            if (!this.blockTabset) {
              this.deleteTabset(tabset.id);
            }

            tabset.tabs.forEach((tab) => {

              chrome.tabs.create({
                'url': tab.url,
                'active': false
              });

            });

          },

          updatedTabset: function(tabset) {
            let store = db.transaction("tabsets", "readwrite").objectStore("tabsets");
            store.put({
              id: tabset.id,
              tabs: tabset.tabs,
              createdAt: tabset.createdAt,
              tabsetName: tabset.tabsetName
            });
          },

          blurInput: function(event) {
            event.target.blur()
          }
        }
      });

      window.vm = new Vue({
        el: '#single-tab-extension',

        data: {
          allTabsets: allTabsetsData,
        }

      });


    }
  }

});